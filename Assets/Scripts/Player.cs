﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private Animation thisAnimation;
    public int force;
    private  int score;
    public GameObject obstacle;
    public Text scoretext;
    void Start()
    {
        thisAnimation = GetComponent<Animation>();
        thisAnimation["Flap_Legacy"].speed = 3;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            thisAnimation.Play();
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
            this.GetComponent<Rigidbody>().AddForce(new Vector3(0,force,0));

        }

        scoretext.text = Obstacle.passedobject.ToString();
      
        if (this.gameObject.transform.position.x >obstacle.transform.position.x)
        {
          
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "TopBoundary")

        {
            
            
        }

       
        if (collision.gameObject.tag == "obstacle")

        {
            SceneManager.LoadScene("GameOver");
        }

       

          

    }

}

